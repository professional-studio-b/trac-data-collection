# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.0.3] - 2019-10-27
### Added
- Usgae and testing documentation

## [0.0.2] - 2019-10-27
### Added
- GPS fix

## [0.0.1] - 2019-09-25
### Added
- GitLab CI Python linting
- Accelerometer recording capability
- Custom logger
