import sys
import logging
from hypothesis import given, note
import hypothesis.strategies as st
from unittest.mock import MagicMock, patch

# Mock the envirophat module
if 'envirophat' not in sys.modules:
    sys.modules['envirophat'] = MagicMock()
import data_collection

# Defaults
DEFAULT_ARGS_DICT = {
    'verbose': False,
    'console_log': False,
    'sample_rate': data_collection.DEFAULT_SAMPLE_RATE,
    'batch_size': data_collection.DEFAULT_BATCH_SIZE,
    'gps_serial_port': data_collection.DEFAULT_GPS_SERIAL_PORT,
    'gps_serial_baudrate': data_collection.DEFAULT_GPS_SERIAL_BAUDRATE,
    'gps_serial_timeout': data_collection.DEFAULT_GPS_SERIAL_TIMEOUT,
    'line': data_collection.DEFAULT_LINE_ID,
    'endpoint': None
}

# Test argument parser
@given(st.lists(st.sampled_from(['--verbose', '-v']), min_size=1))
def test_argparse_verbose_enabled(args):
    parsed_args = vars(data_collection.parse_arguments(args))
    note(parsed_args)

    expected_args = dict(DEFAULT_ARGS_DICT)
    expected_args['verbose'] = True
    assert parsed_args == expected_args


@given(st.lists(st.sampled_from(['--console-log', '-c']), min_size=1))
def test_argparse_console_enabled(args):
    parsed_args = vars(data_collection.parse_arguments(args))
    note(parsed_args)

    expected_args = dict(DEFAULT_ARGS_DICT)
    expected_args['console_log'] = True
    assert parsed_args == expected_args


@given(st.sampled_from(['--sample-rate', '-r']), st.floats(min_value=0))
def test_argparse_sample_rate(flag, rate):
    parsed_args = vars(data_collection.parse_arguments([flag, f'{rate}']))
    note(parsed_args)

    expected_args = dict(DEFAULT_ARGS_DICT)
    expected_args['sample_rate'] = rate
    assert parsed_args == expected_args


@given(st.sampled_from(['--batch-size', '-b']), st.integers(min_value=0))
def test_argparse_batch_size(flag, batch):
    parsed_args = vars(data_collection.parse_arguments([flag, f'{batch}']))
    note(parsed_args)

    expected_args = dict(DEFAULT_ARGS_DICT)
    expected_args['batch_size'] = batch
    assert parsed_args == expected_args


@given(st.sampled_from(['--gps-serial-port', '-gp']), st.text(min_size=1))
def test_argparse_gps_serial_port(flag, port):
    port = port.strip('-')
    parsed_args = vars(data_collection.parse_arguments([flag, f'{port}']))
    note(parsed_args)

    expected_args = dict(DEFAULT_ARGS_DICT)
    expected_args['gps_serial_port'] = port
    assert parsed_args == expected_args


@given(st.sampled_from(['--gps-serial-baudrate', '-gb']),
       st.integers(min_value=0))
def test_argparse_gps_serial_baudrate(flag, baudrate):
    parsed_args = vars(data_collection.parse_arguments([flag, f'{baudrate}']))
    note(parsed_args)

    expected_args = dict(DEFAULT_ARGS_DICT)
    expected_args['gps_serial_baudrate'] = baudrate
    assert parsed_args == expected_args


@given(st.sampled_from(['--gps-serial-timeout', '-gt']),
       st.floats(min_value=0))
def test_argparse_gps_serial_timeout(flag, timeout):
    parsed_args = vars(data_collection.parse_arguments([flag, f'{timeout}']))
    note(parsed_args)

    expected_args = dict(DEFAULT_ARGS_DICT)
    expected_args['gps_serial_timeout'] = timeout
    assert parsed_args == expected_args


# Test configurators
@given(st.booleans(), st.booleans())
def test_configure_logger(verbose, console_log):
    data_collection.configure_logger(verbose, console_log)

    assert data_collection.logger.isEnabledFor(logging.DEBUG) == verbose
    assert ((console_log and len(data_collection.logger.handlers) == 2) or
            (not console_log and len(data_collection.logger.handlers) == 1))


@patch('data_collection.serial')
@given(st.text(), st.integers(min_value=0), st.floats(min_value=0))
def test_configure_gps_serial(mock_serial, serial_port, serial_baudrate,
                              serial_timeout):
    data_collection.configure_gps_serial(serial_port, serial_baudrate,
                                         serial_timeout)
    assert data_collection.gpsSerial == mock_serial.Serial()


# Test data collection
@patch('envirophat.motion.accelerometer')
@given(st.decimals(min_value=-10, max_value=10, places=5),
       st.decimals(min_value=-10, max_value=10, places=5),
       st.decimals(min_value=-10, max_value=10, places=5))
def test_get_current_motion_delta(mock_accel, mock_x, mock_y, mock_z):
    mock_accel.return_value = (mock_x, mock_y, mock_z)
    expectedTuple = (abs(mock_x), abs(mock_y), abs(mock_z-1))
    assert data_collection.get_current_motion_delta() == expectedTuple


def test_get_current_gps():
    # TODO
    pass


@patch('data_collection.get_current_motion_delta')
@patch('data_collection.datetime')
@patch('data_collection.get_current_gps')
@given(st.decimals(min_value=0, max_value=10, places=5),
       st.decimals(min_value=0, max_value=10, places=5),
       st.decimals(min_value=0, max_value=10, places=5),
       st.datetimes(), st.text(), st.text())
def test_datapoint_payload(mock_gps, mock_datetime, mock_motion, mock_x,
                           mock_y, mock_z, mock_date, mock_lat, mock_lon):
    mock_motion.return_value = (mock_x, mock_y, mock_z)
    mock_datetime.utcnow.return_value = mock_date
    mock_gps.return_value = (mock_lat, mock_lon)

    datapoint = data_collection.generate_datapoint_payload()
    assert datapoint['xAccel'] == mock_x
    assert datapoint['yAccel'] == mock_y
    assert datapoint['zAccel'] == mock_z
    assert datapoint['dateTime'] == mock_date.isoformat() + 'Z'
    assert datapoint['gpsLat'] == mock_lat
    assert datapoint['gpsLon'] == mock_lon


# Test sampling
@patch('data_collection.datetime')
@given(st.datetimes(), st.datetimes())
def test_seconds_since_rfc3339_datetime(mock_datetime, mock_current_date,
                                        mock_prev_date):
    mock_datetime.utcnow.return_value = mock_current_date
    mock_datetime.fromisoformat.return_value = mock_prev_date

    mock_prev_str = mock_prev_date.isoformat() + 'Z'
    time_diff = data_collection.seconds_since_rfc3339_datetime(mock_prev_str)
    assert time_diff == (mock_current_date - mock_prev_date).total_seconds()


@patch('data_collection.seconds_since_rfc3339_datetime')
@given(st.decimals(places=5, allow_nan=False), st.floats(min_value=0),
       st.lists(st.fixed_dictionaries({'dateTime': st.datetimes()}),
                min_size=1))
def test_has_sample_rate_lapsed(mock_time_diff, mock_seconds_since,
                                mock_sample_rate, mock_datapoints):
    mock_time_diff.return_value = mock_seconds_since

    lapsed = data_collection.has_sample_rate_lapsed(mock_datapoints,
                                                    mock_sample_rate)
    assert (mock_seconds_since > mock_sample_rate) == lapsed


@given(st.integers(min_value=0),
       st.lists(st.fixed_dictionaries({'dateTime': st.datetimes()}),
                min_size=1))
def test_clear_batch(mock_batch_size, mock_datapoints):
    init_datapoint_length = len(mock_datapoints)
    data_collection.clear_batch(mock_datapoints, mock_batch_size)
    assert (init_datapoint_length >= mock_batch_size) == (not mock_datapoints)
