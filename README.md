# TRAC Data Collection

IoT project for collecting and publishing track data. This script will collect and publish accelerometer values and GPS coordinates.

## Pre-requisites

The following are required to execute this script:

- RaspberryPi with WiFi
- GPS Module NEO-6M
- Enviro pHAT
- Python 3.7
- The python packages specified in [requirements.txt](./requirements.txt)

## Usage

The script has an argument parser which will guide you through the configurable command-line options.
Execute the following to get further help:

```bash
python3 data_collection.py --help
```

Here is an example of a command that will enable debug logging in the console and publish to an API endpoint:

```bash
python3 data_collection.py -cv -e https://ql6d9v0jw8.execute-api.ap-southeast-2.amazonaws.com/trac/collect
```

## Testing

Unit-tests have been implemented using `pytest` and `hypothesis`.
The tests are defined in the [test_data_collection.py](./test_data_collection.py) file.
Additionally, code coverage reporting has also been implemented through the `pytest` plugin `pytest-cov`.
To execute the unit-tests and code coverage reporting, ensure you have the necessary testing requirements specified in [test-requirements.txt](./test-requirements.txt).

The following command will perform all unit-tests and code coverage reporting:

```bash
pytest -vv --html=test_html_report.html --self-contained-html --hypothesis-show-statistics --cov --cov-report=term --cov-report=html
```
