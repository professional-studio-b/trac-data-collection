import argparse
import logging
import serial
import pynmea2
from datetime import datetime
from envirophat import motion
import requests
import json

# Defaults
DEFAULT_SAMPLE_RATE = 0.1  # Sample rate (in seconds)
DEFAULT_BATCH_SIZE = 50  # Batch size to publish data
DEFAULT_GPS_SERIAL_PORT = '/dev/serial0'  # Serial port to listen for GPS
DEFAULT_GPS_SERIAL_BAUDRATE = 9600  # GPS Serial baud rate
DEFAULT_GPS_SERIAL_TIMEOUT = 0.5  # GPS Serial timeout
DEFAULT_LINE_ID = 'T1'  # Line ID

# Custom logger
logger = logging.getLogger(__name__)

# GPS serial
gpsSerial = None

# Request headers
headers = {
    'Content-Type': "application/json",
    'Accept': "/",
    'Cache-Control': "no-cache",
    'Accept-Encoding': "gzip, deflate",
    'Content-Length': "304",
    'Connection': "keep-alive",
    'cache-control': "no-cache"
}


def parse_arguments(args=None):
    # Create argument parser
    parser = argparse.ArgumentParser(description="TRAC collection system.")

    # Configure arguments
    parser.add_argument("-v", "--verbose", help="increase log verbosity",
                        action="store_true")
    parser.add_argument("-c", "--console-log", help="log to the console",
                        action="store_true")
    parser.add_argument("-r", "--sample-rate", type=float,
                        help="rate to sample sensors (in seconds)",
                        default=DEFAULT_SAMPLE_RATE)
    parser.add_argument("-b", "--batch-size", type=int,
                        help="number of datapoints to publish as a batch",
                        default=DEFAULT_BATCH_SIZE)
    parser.add_argument("-gp", "--gps-serial-port",
                        help="serial port to listen on for the GPS device",
                        default=DEFAULT_GPS_SERIAL_PORT)
    parser.add_argument("-gb", "--gps-serial-baudrate", type=int,
                        help="GPS serial baud rate",
                        default=DEFAULT_GPS_SERIAL_BAUDRATE)
    parser.add_argument("-gt", "--gps-serial-timeout", type=float,
                        help="GPS serial timeout",
                        default=DEFAULT_GPS_SERIAL_TIMEOUT)
    parser.add_argument("-l", "--line",
                        help="line ID",
                        default=DEFAULT_LINE_ID)
    parser.add_argument("-e", "--endpoint",
                        help="endpoint to post data to")

    # Parse arguments and return
    return parser.parse_args(args)


def configure_logger(verbose=False, console_log=False):
    # Define logger format
    log_format = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')

    # Create log handlers
    handlers = []
    current_date = datetime.now().strftime('%d.%m.%Y')
    handlers.append(logging.FileHandler(f'data-collection.{current_date}.log'))

    if console_log:
        handlers.append(logging.StreamHandler())

    # Clear all existing handlers
    while logger.handlers:
        logger.removeHandler(logger.handlers[0])

    # Set log handler format and add to the logger
    for handler in handlers:
        handler.setFormatter(log_format)
        logger.addHandler(handler)

    # Set log level
    if verbose:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)


def configure_gps_serial(serial_port, serial_baudrate, serial_timeout):
    global gpsSerial
    gpsSerial = serial.Serial(serial_port, baudrate=serial_baudrate,
                              timeout=serial_timeout)


def generate_datapoint_payload():
    # Get datapoint timestamp in the format defined by RFC3339
    timestamp = datetime.utcnow().isoformat() + 'Z'

    # Fetch accelerometer delta
    (dx, dy, dz) = get_current_motion_delta()

    # Fetch GPS Coordinates
    (lat, lon) = get_current_gps()

    # Create datapoint payload
    datapoint = {
        'dateTime': timestamp,
        'xAccel': dx,
        'yAccel': dy,
        'zAccel': dz,
        'gpsLat': lat,
        'gpsLon': lon
    }

    return datapoint


def get_current_gps():
    global gpsSerial
    nmea_sentences = 6  # Number of sentences in NMEA message

    # Attempt to get GPS fix
    while nmea_sentences:
        nmea_sentence = gpsSerial.readline().decode('UTF-8')

        # Find GPS fix sentence
        if nmea_sentence.find('GGA') > 0:
            nmea_parsed = pynmea2.parse(nmea_sentence)

            # Verify valid GPS fix
            if (nmea_parsed.lat and nmea_parsed.lon and nmea_parsed.lat_dir
                    and nmea_parsed.lon_dir):
                return (f'{nmea_parsed.lat} {nmea_parsed.lat_dir}',
                        f'{nmea_parsed.lon} {nmea_parsed.lon_dir}')
            break
        nmea_sentences -= 1

    # No valid GPS fix was read
    return '0.0', '0.0'


def get_current_motion_delta():
    # Get accelerometer values
    (x, y, z) = motion.accelerometer()
    # Subtract 1g from Z axis to account for gravity and return
    return abs(x), abs(y), abs(z-1)


def seconds_since_rfc3339_datetime(rfc3339_datetime):
    # Convert RFC3339 date time string to datetime object
    parsed_datetime = datetime.fromisoformat(rfc3339_datetime.strip('Z'))

    # Calculate current difference
    datetime_diff = datetime.utcnow() - parsed_datetime
    return datetime_diff.total_seconds()


def has_sample_rate_lapsed(datapoints, sample_rate):
    # Get latest datapoint
    datapoint = datapoints[len(datapoints)-1]

    # Get seconds since last datapoint
    lapsed_seconds = seconds_since_rfc3339_datetime(datapoint['dateTime'])

    # Return comparison result with sample_rate
    return lapsed_seconds > sample_rate


def clear_batch(datapoints, batch_size):
    if len(datapoints) >= batch_size:
        datapoints.clear()


def main():
    # Initialisation
    args = parse_arguments()
    configure_logger(args.verbose, args.console_log)
    logger.info('Starting TRAC collection system')

    logger.debug(f'Verbose: {args.verbose}')
    logger.debug(f'Console Logging: {args.console_log}')
    logger.info(f'Sample Rate: {args.sample_rate}')
    logger.info(f'Batch Size: {args.batch_size}')
    logger.info(f'GPS Serial Port: {args.gps_serial_port}')
    logger.info(f'GPS Serial Baudrate: {args.gps_serial_baudrate}')
    logger.info(f'GPS Serial Timeout: {args.gps_serial_timeout}')

    logger.debug('Configuring GPS serial connection')
    configure_gps_serial(args.gps_serial_port, args.gps_serial_baudrate,
                         args.gps_serial_timeout)

    logger.debug('Initialising empty datapoint list')
    datapoints = []

    # Main loop
    logger.info('Beginning main loop')
    while True:
        # Get datapoints at specified sample rate
        if ((not datapoints) or
           has_sample_rate_lapsed(datapoints, args.sample_rate)):
            logger.debug(f'Current batch size: {len(datapoints)}')
            # Clear batch if necessary
            clear_batch(datapoints, args.batch_size)

            # Get datapoint sample
            datapoint = generate_datapoint_payload()
            logger.debug(datapoint)
            datapoints.append(datapoint)

            # Pulish datapoint
            if args.endpoint:
                payload = {
                    "lineId": args.line,
                    "samples": [datapoint]
                }
                logger.debug(f'Publishing payload: {payload}')

                response = requests.request("POST", args.endpoint,
                                            data=json.dumps(payload),
                                            headers=headers)
                logger.debug(f'Post response: {response.text}')


if __name__ == "__main__":
    main()
